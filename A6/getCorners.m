
function [r,c] = getCorners(I2, method, sigma, thresh, alpha)
    switch nargin
        case 2
            sigma = 1; thresh = 0.3; alpha = 0.004;
        case 3
            thresh = 0.3; alpha = 0.004;
        case 4
            alpha = 0.004;
    end
    %[r,c] = getcorners(I, method, sigma, thresh, alpha);
    I = im2double(imread(I2));

    %Compute Ix and Iy
    dx = [-1 -1 -1; 0 0 0; 1 1 1];
    dy = dx';
    Ix = imfilter(I, dx, 'same');
    Iy = imfilter(I, dy, 'same');

    %Compute Sxx, Syy, and Sxy
    w = fspecial('gaussian', round(6*sigma), sigma);
    Sxx = conv2(Ix.^2, w, 'same');
    Syy = conv2(Iy.^2, w, 'same');
    Sxy = conv2(Ix.*Iy, w, 'same');

    %Compute R
    if( method == 'h')
        R = (Sxx.*Syy - Sxy.^2)-alpha*((Sxx+Syy).^2); %Harris Measure
    else
        R = (Sxx.*Syy-Sxy.^2)./(Sxx+Syy);
    end
    %Threshold R and find local maxima
    N = 9;
    Rdilate = imdilate(R, strel('disk', N));    %Grey-scale dilate
    corners = (R>thresh) & (R== Rdilate); %Find local maxima

    %overlaying corners on original image
    [r,c] = find(corners); %Find row, col coords
    figure, imshow(I), hold on;
     for i = 1:size(r)
        rectangle('Position',[c(i)-2,r(i)-2,4,4],'EdgeColor','r');
    end
   % plot(c, r, 'rs');
end
