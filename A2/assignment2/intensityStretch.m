%% Question 3
%Function is tested in testfunctions.m
%Create function intensityStretch that changes contrast of given images
%Five arguemnts (inputImage,r1, r2, s1, s2)
%Must check that 
%r1>= min intesnity of I
%r2<= max intrnsity of I
%s1 >= 0
%s2<=255

%Point operations are intensity transformations
%This point operation increases the contrast by creating
%A larger range of pixel intensities using a linear mapping function
%Learned how to do this through https://homepages.inf.ed.ac.uk/rbf/HIPR2/stretch.htm
function [output] = intensityStretch(image, r1, r2, s1, s2)
    output = grayscaleCheck(image);
    r1 = double(r1);
    r2 = double(r2);
    s1 = double(s1);
    s2 = double(s2);
    %Checking if the parameters are valid
    if r1 >= min(min(output)) && r2 <= max(max(output)) && s1 >= 0 && s2 <= 255
        %s1 and s2 are the lower and upper levels to be normalized to
        %The lower and upper level range we chose or the min and max
        %present in the image is usually used
        output = (output-r1)*((s2-s1)/(r2-r1))+s1;
    end
end

