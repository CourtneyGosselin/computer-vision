%Function Test File

%% Grayscale Function Test
I = imread('mountain.jpg');
I = grayscaleCheck(I);
figure, imshow(I);

%% CorrectBrightness Function Test
I = imread('moon.jpg');
figure, imshow(I);
figure, imshow(correctBrightness(I, 80, false));
figure, imshow(correctBrightness(I, 80, true));

%% Perspective Wrap Function Test
I = imread('prespectiveCorrect.jpg');
figure, imshow(perspectiveWrap(I));

%% Intensity Stretch Function Test
I = imread('baby_faceHidden.png');
I2 = intensityStretch(I,2,50,2,200);
imshow(I), figure, imshow(I2);