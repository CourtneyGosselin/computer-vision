%% Question 2
%Function is tested in testfunctions.m

% A) To understand the science of the tools an individual has to 
%understand that humans can understand that a 2D image of a 3D 
%object can tell that the lines that are converging in the 2D image
%are parallel in reality. Humans take in the contect of what the image
%is. Like a set of train tracks do not converge to a single point.
%We understand that they are running parrallel.Perspective tools 
%take this into account these tools take the perspective information
%provided by the userto provide the new theoretical image. 

function [perspectiveFix] = perspectiveWrap(perspectiveWrapOriginalImage)
    %Show the original image
    imshow(perspectiveWrapOriginalImage);
    perspectiveWrapOriginalImage = grayscaleCheck(perspectiveWrapOriginalImage);
    figure, imshow(perspectiveWrapOriginalImage);
    %Then reads the (x,y) of four mouse clicks on input image 
    %Using ginput. Clicks must be in same order as provided in lab
    %The points should be transformed to form a rectangle   
    %Everytime a user clicks a point a small rectangle is drawn on image
    %Top left
    [x1, y1] = ginput(1);
    rectangle('Position', [x1(1)-2, y1(1)-2, 4, 4], 'EdgeColor', 'r');
    %top Right
    [x2, y2] = ginput(1);
    rectangle('Position', [x2(1)-2, y2(1)-2, 4, 4], 'EdgeColor', 'r');
    %Bottom left
    [x3, y3] = ginput(1);
    rectangle('Position', [x3(1)-2, y3(1)-2, 4, 4], 'EdgeColor', 'r');
    %Bottom Right
    [x4, y4] = ginput(1);
    rectangle('Position', [x4(1)-2, y4(1)-2, 4, 4], 'EdgeColor', 'r');
    %registration Using affine transformation
    %Refer to the points found and make a rectangle out of these points 
    %The fixed variable is the points from the original
    fixed = [x1, y1; x2, y2; x3, y3; x4, y4];
    changed = [(x1+x3)/2,(y1+y2)/2;(x2+x4)/2,(y1+y2)/2;(x1+x3)/2,(y3+y4)/2;(x2+x4)/2,(y3+y4)/2];
    %Make a transformation based off the moving points and fixed points
    T = fitgeotrans(fixed, changed, 'affine');
    perspectiveFix = imwarp(perspectiveWrapOriginalImage, T);   
end
