%%Grayscale Check function if it is not convert and return
%Function is tested in testfunctions.m
function [imageOut] = grayscaleCheck(image)
    if size(image) > 1   %Check number of dimensions RBG is greater than 2
        imageOut = rgb2gray(image);
    end
end 