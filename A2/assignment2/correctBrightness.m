%% Question 1
%Function is tested in testfunctions.m
%Check if image is grayscale if it is not convert it
%Correct the brightness of a given image
%Create function that takes arguments an image and a number between 1-100
%Third parameter indicating whether the correction is to darken or lighten
%Returns the corrected grayscale image
function [correctedImage] = correctBrightness(image, percent, brighten)
    beforeImage = grayscaleCheck(image);
    if brighten     %The image is being brightened
    %here could make it positive below negative
    correctedImage = beforeImage*(1+(percent/100));
    
    else  %The image is being darkened
    %here
    correctedImage = beforeImage/(1+(percent/100));
    end
end



