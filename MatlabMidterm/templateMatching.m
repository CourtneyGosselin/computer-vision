
function [I]= templateMatching(I, template)
    
    for d = 90:90:360 %Change the different rotation option 90 up
        for p = 1:2:8 %Increase the size of the original image
            I = imrotate(I,d);
            I = resize(I, p);
            g = normxcorr2(template,I); 
            % remove the padding added in the result image
            g = imcrop(g,[(size(w,2)-1)/2+1 (size(w,1)-1)/2+1 size(I,2)-1 size(I,1)-1]);
            figure, surf(g), shading flat, title('normalized cross correlation')
            %find the max (at which there is a perfect match)
            cmax = max(g(:));
            [corY corX]=find(g==cmax);
            % put a mark at the matching location
            figure, imshow(I);
            line([corX-10 corX+10], [corY corY], 'Color', 'r');
            line([corX corX], [corY-10 corY+10], 'Color', 'r');
        end
    end
end