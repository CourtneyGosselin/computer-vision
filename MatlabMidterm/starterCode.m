%% Starter Code Provided for Midterm1 Practical Component
% You can copy one ore more statements from the starter code (or any code in the notes,textbook,etc) into your programs.
% Or you can ignore the code provided below and write your own code from scratch.

%% Starter Code for Q1:

% read the image
I = imread('moon.tif'); %change to moon.tif
I2 = I;
I = im2double(I); 
% get the x and y first derivatives
wx = [-1 -2 -1; 0 0 0; 1 2 1];
wy = wx';
gx = imfilter(I,wx);
gy = imfilter(I,wy);
%get the Gradient and display alpha
M = (gx .^2 + gy.^2) .^0.5;

alpha = atan2(abs(gx),abs(gy));
alpha = alpha >100;
[Gx, Gy] = imgradient(I2);
imshow(Gx);
imshow(alpha,[]), title('alpha');


%% Starter Code for Q2:
I = imread('okanagan.jpg');
%Reduce noise
IM = medfilt2(I);
imshow(IM), title('test2');

moving = IM;

[x1,y1]=ginput(1);
rectangle('Position',[x1 y1 4 4],'EdgeColor','r');

[x2,y2]=ginput(1);
rectangle('Position',[x2 y2 4 4],'EdgeColor','r');

% adjust perspective
angle = atan2(x1, y2)*180/pi;
moved = imrotate(moving, angle);
%show result
imshow(moved);

%dreamy effect
w2 = fspecial('gaussian', 15, 3);
I_blurred = imfilter(moved, w2);
I_dreamy = 0.5 * moved + 0.5 *I_blurred;
imshow(I_dreamy);


%% Starter Code for Q3:
I = imread('cameraman.tif');
figure, imshow(I);
template = imread('template.tif');
% Normalized Cross Correlation (search for w)
figure, imshow(templateMatching(I, template));



%%Not enough time
