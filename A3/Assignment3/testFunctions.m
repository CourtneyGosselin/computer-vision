%Test Functions for Assignment 3

%% Question 1: Histogram Equalization Test
I = imread('fig0315c.bmp');
I2 = my_histeq(I);
I3 = histeq(I);
subplot(1,3,1), imshow(I), title('Original');
subplot(1,3,2), imshow(I2), title('Using my equalizer');
subplot(1,3,3), imshow(I3), title('Using Matlab histeq');

%% Question 2: Adding Shadow Test
I = imread('coins.png');
I2 = add_shadow(I, 90, 5, 10); %Threshold = 90, blur amount = 5, distance = 10
figure, subplot(1,2,1), imshow(I), title('Original');
subplot(1,2,2), imshow(I2), title('With Shadows');