%A way to add shadows
function [output] = add_shadow(I, threshold, blur, distance)
    %Check grayscale of image
    I = grayscaleCheck(I);
    %Use thresholding to seperate foreground and background
    I2 = I;
   
    temp = imtranslate(I2,[distance,distance]);  %Determine shadow locations
    figure, imshow(temp), title('distance added');
    Itemp = temp;
    temp(Itemp>threshold) = 0;
    temp(Itemp<threshold) = 255;
    figure, imshow(temp), title('inverse');
   
    %Blur I2
    gaussian = fspecial('gaussian', 20, blur);
    I2 = imfilter(temp, gaussian);
    figure, imshow(I2), title('blur');
    %figure, imshow(I2), title('I2-Blur');
    %Break the original image into two images
    %I3 with the items drawn on black background
    I3 = I;
    I3(I<threshold)=0;  %I3 is the coins;
    figure, imshow(I3), title('I3');
    %I4 Only the background, foreground replaced with black
    I4 = I;
    I4(I>threshold)=0;  %I4 is the background
    figure, imshow(I4), title('I4');
    
    %Output= combine all 3 images
    I2 = im2double(I2);
    I3 = im2double(I3);
    I4 = im2double(I4);
   
    output = immultiply(I2,I4);
    output(I3~=0)=0;
    output = imadd(output,I3);
end