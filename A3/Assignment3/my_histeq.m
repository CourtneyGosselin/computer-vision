function [output] = my_histeq(I)
    %Check grayscale
    I = grayscaleCheck(I);
    %---Compute the pdf---
    %Get Histogram of the image Hr(r) = nr
    histogram = zeros(1,255);
    for r = 1:size(I,1)
        for c = 1:size(I,2)
            histogram(I(r,c))= histogram(I(r,c))+1;
        end
    end
    %Then to get pdf by dividing the Hr(r) by MN
    pdf = histogram/(size(I,1)*size(I,2));
    %---Get sk of output image---
    %For each intensity rk, Ccompute the cdf by summing prior PDFs
    cdf =  cumsum(pdf);
    %Find T(r) by multiplying the CDF by (L-1)
    s = cdf*255;
    output = uint8(s(I));   
end