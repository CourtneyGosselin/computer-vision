% sample use:
% I = imread('coins.png'); %must be grayscale from 0 to 255
% I2 = add_shadow(I, 90, 5, 10);
% subplot(1,2,1), imshow(I), title('original');
% subplot(1,2,2), imshow(I2), title('with shadows');
function Ifinal = add_shadow(I, threshold, blur_amount, distance)
% convert to double 
I = double(I);
M = size(I,1);
N = size(I,2);

% identify the forground based on the given threshold
mask = I>threshold;    

% get shadow image (0=black for foreground items, 1=white background)
shadow = double(1-mask);

% Translate shadow by the amount of distance 
% (note that using affine, imwrap will refit the image removing the black margins - see https://www.mathworks.com/help/images/perform-a-2-d-translation-transformation.html
top_margin = ones(distance, N);
left_margin = ones(M,distance);
shadow = [top_margin; shadow(1:M-distance,:)] ;  % insert top margin
shadow = [left_margin, shadow(:,1:N-distance)];  % insert left margin

% Blur shadow image 
w = fspecial('gaussian', 5 * blur_amount, blur_amount);
shadow = imfilter(shadow, w, 'replicate');%blur shadow

% apply shadow to background only
Iback = I .* (1 - mask);  % background only (foreground items = 0)
Iback = Iback .* shadow;  % shadow applied (darken the background at shadow areas)

% put in the foreground items 
Icoins = I .* mask; % forground item only (background=0)
Ifinal = uint8(Icoins + Iback);
end

