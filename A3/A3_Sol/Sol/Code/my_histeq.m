%input is a grayscale image. output is histogram equalized image
function result = my_histeq(I)
M = size(I,1);  % number of rows
N = size(I,2);  % number of cols
%% get the histogram as a PDF
count = zeros(256,1);
for r=1:M
    for c=1:N
        count(I(r,c)+1) =  count(I(r,c)+1) + 1;
    end
end
pdf = count / (M*N);
%% get the CDF as an integer from 0 to 255
cdf = cumsum(pdf);
s = uint8(cdf * 255); 
%% get the equalized historgram
result = s(I);  %use s as transformation function
%{  
    % The above statement is the same as:
    result=I;   
    for r=1:M
        for c=1:N
         result(r,c)=s(I(r,c));   
        end
    end
%}
end