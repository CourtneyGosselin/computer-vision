%%Note: Run each section individually 2e is broken
%% Question 1
%See word doc


%% Question 2
%2a
%Read the image into varibale I1
%Display the image on the screen
clear;
I1 = imread('okanagan.jpg');
imshow(I1, []);


%2b
%Display red, green, and blue channels
%Properly title channels
figure, imshow(I1(:,:,1), []);
title('Red Channel');

figure, imshow(I1(:,:,2), []);
title('Green Channel');

figure, imshow(I1(:,:,3), []);
title('Blue Channel');

%% 2c
%Convert the image to grayscale
%Save output to I1 show new image with title I1
I1 = rgb2gray(I1);
Ori = I1;
figure, imshow(I1, []);
title('I1');

%2d
%Display the size of I1
[m, n] = size(I1);
%Format text and another to display text on console
txt = sprintf('The image I1 has %d rows %d of columns.', m, n);
fprintf(txt);

%% 2e
%What is the class of I1? Hint: Use whos or check workspace
whos;
%Answer: The class of I1 is uint8, which is an unsigned 8 bit integer

%% 2f 
%Display the maximum and mimumum pixel intensities in I1
maxVal = max(max(I1(:)));
minVal = min(min(I1(:)));

txt = sprintf('Gray level range in I1: %d to %d. \n', minVal, maxVal);
fprintf(txt);


%Create new grayscale image I2 from I1 
%With gray level multiplied by 4
%Display I2 the max and min pixel values again 

I2 = I1*4;
maxVal = max(max(I2(:)));
minVal = min(min(I2(:)));

txt = sprintf('Gray level range in I2: %d to %d. \n', minVal, maxVal);
fprintf(txt);


%% Question 3
%Read the image into I2 
%Then insert I2 inro I1 bottom left corner
%Label this I4 with a suitable title
%Hint: Use the size function and the (:) operator 
I2 = imread('cameraman.tif');
figure, imshow(I2, []);

[m, n] = size(I2);
txt = sprintf('The image I2 has %d rows %d of columns.', m, n);
fprintf(txt);

x = 400-256;
y = 1;

 
 s = size(I2);
 I1(x:x+s(1)-1, y:y+s(2)-1, :) = I2; %this is why the ori is needed
 imshow(I1);
 
 
%% Question 4
%Seperate foreground (mountains, field and lake) 
%from background (sky)
%Output = I4 which has black sky done by creatin I$ 
%Pixels above 175 are set to 0
%Title: I4: Background (sky) removed!
I4 = Ori; %assign I4 the grayscale of the original image 
I4(I4>175) = 0;
figure, imshow(I4);
title('I4: Background (sky) removed!');
 

