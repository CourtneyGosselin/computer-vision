%%q1
clear; clc; close all;
I = imread('Q1.png');
%figure, imshow(I), title('Original');

SE = strel('square', 7);
S = strel('disk', 2);
I2 = imopen(I, SE);
I2 = imopen(I2, S);
I2 = im2bw(I2);
%figure, imshow(I2), title('Morph');

BW = edge(I2, 'canny');
%figure, imshow(BW), title('InitialEdge');

[H, T, R] = hough(BW);
peaks = houghpeaks(H, 300, 'Threshold', 1);
lines = houghlines(BW, T, R, peaks, 'FillGap', 7, 'MinLength', 1);
H = mat2gray(H);
%figure, imshow(H, 'InitialMagnification', 'fit'), axis normal;

figure, imshow(I, []);
for k = 1:length(lines)
    xy=[lines(k).point1; lines(k).point2];
    line(xy(:,1), xy(:,2), 'LineWidth', 3, 'Color', 'r');
end


