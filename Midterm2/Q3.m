%%Bonus
clear; clc; close all;
I = imread('Q3.png');
I2 = I(:,:,1);
I2 = im2bw(I2);
%figure, imshow(I), title('Original');
%figure, imshow(I2), title('Original');

pad = padarray(I2, [1 1], 1);
[L, num] = bwlabel(pad, 8);

new = bwpropfilt(pad, 'Area', [20000, 500000]);
figure, imshow(I), title('Stop');

[blobs, N] = bwlabel(new); %Find blobs
props = regionprops(blobs);
areas = cat(2, props(:).Area);
indeces = find(areas>50);
fXL= props(indeces);


for i = 1:length(props)
    rectangle('Position', props(i).BoundingBox, 'EdgeColor', 'y', 'LineWidth', 2);
end