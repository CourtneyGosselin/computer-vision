clear; clc; close all;
I=imread('Q2.png');
I(I<90) = 0;
I(I>90) = 255;
%figure, imshow(I), title('Original');
SE = strel('square', 1);
SE2 = strel('square', 1);
I2 = imclose(I, SE2);
I2 = imopen(I2, SE);

%figure, imshow(I2), title('OPEN');
%%
I3 = I2;
I3(I3>90) = 255;
%figure, imshow(I3), title('Thresh');

I4 = I3;
SE2 = strel('square', 3);
SE = strel('square', 2);
I4 = imclose(I3, SE2);
I4 = imopen(I4, SE);
SE2 = strel('square', 1);
SE = strel('square', 2);
I4 = imclose(I4, SE2);
I4 = imopen(I4, SE);

SE2 = strel('square', 3);
SE = strel('square', 3);
I4 = imopen(I4, SE);
I4 = imclose(I4, SE2);
%figure, imshow(I4), title('Close');

%%
I5 = I4;
I5(I5<90) = 0;
I5 = im2bw(I5);
%figure, imshow(I5), title('BW');

pad = padarray(I5, [1 1], 1);
[L, num] = bwlabel(pad, 8);

dot = bwpropfilt(pad, 'Area', [1, 60]);
%figure, imshow(dot), title('dot');

i = bwpropfilt(pad, 'Area', [61, 240]);
%figure, imshow(i), title('The Letter I');

others = bwpropfilt(pad, 'Area', [241, 700]);
%figure, imshow(others), title('Others');

coloured= mat2gray(i);
coloured(:,:,2)= mat2gray(others);
coloured(:,:,3)= mat2gray(dot);
figure, imshow(coloured), title('Coloured');

[blobs, N] = bwlabel(i); %Find blobs
props = regionprops(blobs);
areas = cat(2, props(:).Area);
indeces = find(areas>50);
fXL= props(indeces);

[L, Numi] = bwlabel(i);
[L, Numdot]= bwlabel(dot);
[L, NumOthers] = bwlabel(others);

for i = 1:length(props)
    rectangle('Position', props(i).BoundingBox, 'EdgeColor', 'g', 'LineWidth', 1);
end

title(sprintf(' %d of the letter I, %d Other letters, %d (.)', Numi, NumOthers, Numdot));