%% Question 3
I2 = imread('money.png');
I= im2bw(I2);
I = imdilate(I, strel('disk', 5));
temp = I;
%imshow(I);

%Create SE to
SE = strel('disk', 100);
E = strel('disk', 60);
%Remove items smaller thean SE
O = imerode(imopen(I, SE), E);
%figure, imshow(O), title('toonies');
%Identify blobs
[blobs, N] = bwlabel(O); %Find blobs
props = regionprops(blobs);
toonie = N;
%figure, imshow(I), title(toonies);


%Create SE to
SE = strel('disk', 70);
E = strel('disk', 83);
%Remove items smaller thean SE
O2 = imerode(imopen(I, SE)-O, E);
%figure, imshow(O2), title('loonies');
%Identify blobs
[blobs, N] = bwlabel(O2); %Find blobs
props = regionprops(blobs);
loonie = N;
%figure, imshow(I), title(loonies);

%Create SE to
SE = strel('disk', 70);
E = strel('disk', 60);
%Remove items smaller thean SE
O3 = imerode(imopen(I, SE)-O2-O, E);
%figure, imshow(O3), title('quarters');
%Identify blobs
[blobs, N] = bwlabel(O3); %Find blobs
props = regionprops(blobs);
quarter = N;
toonies = toonie*2;
loonies = loonie;
quarters = quarter*.25;
money = num2str((toonies+loonies+quarters), '%4.2f');
figure, imshow(I2), title(['There is $' money ' in this image.']);
