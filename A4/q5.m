%% Question 5
I = imread('445_descr.png');
imshow(I);
bg = imopen(imclose(I,strel('square',3)),strel('square',12));
toptxt = I - bg;
toptxt(toptxt>10) = 255;
toptxt(250:715,:) = 0;

SE = strel('square', 4);
for i = 1:20
    O = imopen(I, SE);
    O = imclose(O, SE);
    O = imerode(O, SE);
    O = imdilate(O, SE);
end
SE = strel('square', 8);
for i = 1:10
    O = imopen(O, SE);
    O = imclose(O, SE);
    O = imerode(O, SE);
    O = imdilate(O, SE);
end
SE = strel('square', 8);
for i = 1:10
    O = imopen(O, SE);
    O = imclose(O, SE);
    O = imerode(O, SE);
    O = imdilate(O, SE);
end
SE = strel('square', 12);
for i = 1:10
    O = imopen(O, SE);
    O = imclose(O, SE);
    O = imerode(O, SE);
    O = imdilate(O, SE);
end
SE = strel('square', 13);
for i = 1:10
    O = imopen(O, SE);
    O = imclose(O, SE);
    O = imerode(O, SE);
    O = imdilate(O, SE);
end

background = O;
%{
LB = strel('disk', 2);
UB = strel('disk', 100);
bin = im2bw(I);
removeWhiteFont = xor(imopen(bin,LB),  imopen(bin,UB));
%}
figure, imshow(background), title('background');
bottomtxt = imcomplement(I)-imcomplement(O);
bottomtxt(bottomtxt>1) = 255;
bottomtxt(1:250,:) = 0;
text = imcomplement(imdilate(max(bottomtxt,toptxt),strel('square',2)));
figure, imshow(text), title('Text');


