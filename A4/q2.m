%% Question 2
I = imread('coop.png');
temp = I;
imshow(I);

%Create SE to
SE = strel('disk', 36);
E = strel('disk', 10);
%Remove items smaller thean SE
O = imopen(I, SE);
%figure, imshow(O, []);

%Identify blobs
[blobs, N] = bwlabel(O); %Find blobs
props = regionprops(blobs);
chickens = num2str(N);
%figure, imshow(I), title([num2str(N) ' found Chickens']);

%Create SE to find eggs
SE = strel('disk', 28);
%Remove items smaller thean SE
O2 = imopen(I, SE);
O2f = imerode((O2-O), E);
%figure, imshow(O2f, []), title('eggs');

%Identify blob eggs
[blobs, N] = bwlabel(O2f); %Find blobs
props = regionprops(blobs);
eggs =  num2str(N);
%figure(1), title([eggs ' eggs (identified below), and ' chickens ' chickens']);
areas = cat(2, props(:).Area);
indeces = find(areas>2000);
fXL= props(indeces);
for i = 1:length(props)
    x0 = fXL(i).Centroid(1); y0 = fXL(i).Centroid(2);
    line([x0-10, x0+10], [y0, y0], 'Color', 'r', 'LineWidth', 3);
    line([x0, x0], [y0-10, y0+10], 'Color', 'r', 'LineWidth', 3);
    rectangle('Position', props(i).BoundingBox, 'EdgeColor', 'r', 'LineWidth', 3);
end

%Create SE to find big circles
SE = strel('disk', 25);
%Remove items smaller than SE
O3 = imopen(I, SE);
O3f = imerode((O3-O2), E);
%figure, imshow(O3f, []), title('big circles');

%Identify blob eggs
[blobs, N] = bwlabel(O3f); %Find blobs
props = regionprops(blobs);
bigcircles =  num2str(N);
%figure(1), title([bigcircles ' big circles, ' eggs ' eggs (identified below), and ' chickens ' chickens']);


%Create SE to find small circles
SE = strel('disk', 18);
E = strel('disk', 15);
%Remove items smaller than SE
O4 = imopen(I, SE);
O4f = imerode((O4-O3), E);
%figure, imshow(O4f, []), title('small circles');

%Identify blob eggs
[blobs, N] = bwlabel(O4f); %Find blobs
props = regionprops(blobs);
smallcircles =  num2str(N);
figure(1), title([smallcircles ' small cricles, ' bigcircles ' big circles, ' eggs ' eggs (identified below), and ' chickens ' chickens']);
