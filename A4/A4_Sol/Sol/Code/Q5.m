clean;
I=imread('445_descr.png');
%% break to two parts (light text, dark text)
Itop = I(1:250,:);
Ibottom = I(251:end,:);
%% find background
s=strel('disk',3);
backTop=imopen(Itop,s);
backBottom=imclose(Ibottom,s);
back=[backTop;backBottom];
imshow(back);
%% find the abs difference to remove background 
diff = imabsdiff(I,back);
%% Threshold to find the text
text = diff>1;
text = imcomplement(text);
figure, imshow(text);
