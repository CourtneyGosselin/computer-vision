I = imread('money2.png');
imshow(imresize(I,0.25));
 
% convert to BW 
I = im2bw(I);
 
% close holes
s = strel('disk', 10,8);
I = imclose(I, s);
 
% separate coins
s = strel('disk', 65,8);
I = imerode(I, s);
 
% count all coins
[L,c1] = bwlabel(I);        %c1 = toonies+loonies+quarters  
 
% remove quarters and count loonies and toonies
s = strel('disk', 35,8);
I2 = imopen(I,s);   
[L,c2] = bwlabel(I2);       % c2 = toonies+loonies
 
% remove loonies and count toonies
s = strel('disk', 50);
I2 = imopen(I,s);           
[L,c3] = bwlabel(I2);  % c3 = toonies only 
 
% display results
total = c3 * 2 + (c2-c3) + (c1-c2)*.25;
title(sprintf('There is $%.2f in this image.', total));

%another solution could be based on blob areas