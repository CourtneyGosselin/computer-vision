I = imread('particles.png');
% pad I with 1s so that all particles merged with boundary 
% are seen as 1 connected component
I2 = padarray(I,[1 1],true);   
% identify connected components
[L N] = bwlabel(I2);
props = regionprops(L);
allAreas = cat(2,props.Area);
% extract edge particles
Iedge= L==1;    % edge blob has label L=1
Iedge = Iedge(2:end-1,2:end-1);   % remove padding
figure, imshow(Iedge), title('particles merged with the boundary');
% extract nonoverlapping particles based on blob areas
idxSingleParticles = find(allAreas>230 & allAreas<275);
Ismall=zeros(size(I2));
for i = idxSingleParticles  
    Ismall = or(Ismall, L==i);
end
Ismall = Ismall(2:end-1,2:end-1);   % remove padding
figure, imshow(Ismall), title('nonoverlapping particles');
% extract the remaining particles
Irest = I - Iedge - Ismall;
figure, imshow(Irest), title('overlapping particles');
% merge all three layers into one color image
Icolor(:,:,1)=uint8(Iedge)*255;
Icolor(:,:,2)=uint8(Ismall)*255;
Icolor(:,:,3)=uint8(Irest)*255;
figure, imshow(Icolor), title('3 classes of particles');

