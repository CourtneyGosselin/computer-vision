I = imread('coop.png');
imshow(I);
 
%count all items (circles, eggs, chicken)
s = strel('disk', 15);
I2 = imopen(I,s);           % I2 has no lines
[L,count1] = bwlabel(I2);   % count1 = all circles + eggs + chicken
 
%count big circles, eggs, chicken
s = strel('disk', 25);
I2 = imopen(I,s);           % I3 has no lines, no small circles
[L,count2] = bwlabel(I2);   % count2 = big circles + eggs + chicken
 
s = strel('disk', 30);
I2 = imopen(I,s);           % I3 has no lines, no small circles
[L,count3] = bwlabel(I2);   % count3 = eggs + chicken
 
f = regionprops(L); %will use it later to draw '+' inside the eggs
 
s = strel('disk', 40);
I2 = imopen(I,s);   %I3 has no lines, no small circles
[L,count4] = bwlabel(I2);   % count4 = chicken only
 
%draw cross inside the eggs
areas = cat(2, f(:).Area);
indeces = find(areas<7000);
f_eggs = f(indeces);
for i = 1 : numel(f_eggs)
    x0 = f_eggs(i).Centroid(1);  y0 = f_eggs(i).Centroid(2);
    line([x0-10 x0+10], [y0 y0], 'Color', 'r', 'LineWidth',3);
    line([x0 x0], [y0-10 y0+10], 'Color', 'r', 'LineWidth',3);
    rectangle('Position', f_eggs(i).BoundingBox, 'EdgeColor','r', 'LineWidth',3);
end
 
%display the count of every group
title(sprintf('There are %d small circles, %d big circles, %d eggs (identified below), and %d chicken', count1-count2, count2-count3, count3-count4, count4));
