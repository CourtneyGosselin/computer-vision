%% Question 4
I = imread('particles.png');
temp = I;
%imshow(I);
%[rows columns]=size(I );

%{ 
L = bwlabel(I);

f = regionprops(L);
areas = cat(2, f(:).Area);
indeces = find(areas > 350);
fXL = f(indeces);
m = numel(fXL);

for i = 1:m
    rectangle('Position', fXL(i).BoundingBox, 'Curvature', [.8 .8], 'EdgeColor', 'b', 'FaceColor', 'b');
end


LB =  230;
UB = 275;
free = xor(bwareaopen(I,LB),  bwareaopen(I,UB));
figure, imshow(free), title('nonoverlapping particles');
%}
pad = padarray(I, [1 1], 1);
[L, num] = bwlabel(pad, 8);
border = bwpropfilt(pad, 'Area', [6000, 10000]);
figure, imshow(border), title('particles merged with the boundry');

over = bwpropfilt(pad, 'Area', [275, 5999]);
figure, imshow(over), title('overlapping particles');

free = bwpropfilt(pad, 'Area', [0, 275]);
figure, imshow(free), title('nonoverlapping particles');

coloured= mat2gray(border);
coloured(:,:,2)= mat2gray(free);
coloured(:,:,3)= mat2gray(over);
figure, imshow(coloured), title('3 classes of particles');