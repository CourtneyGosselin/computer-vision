function [Iout] = myCanny(I, T, sigma)
   %% Snooth the input with Gaussian Filter
   I = im2double(I);
   w = fspecial('gaussian', sigma*6, sigma);
   I2 = im2double(imfilter(I, w));
   figure, imshow(I2), title('Gaussian Filter Applied');
   Iout = T;
   
   %% Compute the Gradient Magnitude and Angle/Direction 
   %Using sobel
   wy = [-1, 0, 1; -2, 0, 2; -1, 0, 1];
   wx = wy';
   
   %Apply filter
   gx = imfilter(I2, wx);
   gy = imfilter(I2, wy);
   
   %Compute and display gradient
   M = (gx .^ 2 + gy .^ 2) .^ 0.5;
   figure, imshow(M, []), title('Gradient Magnitude');
   
   %Compute Angle
   alpha = atan2(gx, gy);
   figure, imshow(alpha, []), title('\alpha:[-180, 180]');
   
   %Find 4 Directional Gradient 
   alpha = rad2deg(alpha);
   alpha4 = zeros(size(alpha));
   %Used diagram from slide 58 edge detection and group discussion in lab 
   alpha4((alpha<= 22.5 & alpha>=-22.5) | (alpha>157.5 & alpha <= 180) | (alpha<-157.5 & alpha>=-180)) = 0;
   alpha4((alpha>22.5 & alpha<=67.5) | (alpha>=-157.5 & alpha<=-112.5)) = 45;
   alpha4((alpha>67.5 & alpha <=112.5) | (alpha>=-112.5 & alpha<-67.5)) = 90;
   alpha4((alpha>112.5 & alpha<= 157.5) | (alpha>=-67.5 & alpha<-22.5))= 135;
  
   figure, imshow(alpha4, []), title('\alpha: 4 directions');
  
   
   %% Apply Nonmaxima Suppression 
   %The for loop checks every pixel
   Img = zeros(size(M));
   for y = 2:size(I, 2)-1
       for x = 2:size(I, 1)-1
           %Ifs check for which direction and set it to 1
           if(alpha4(x,y) ==135)
               if((M(x+1, y+1)<= M(x,y))&&(M(x-1, y-1)<=M(x,y)))
                   Img(x, y)=1;
               end
           elseif(alpha4(x,y)==90)
               if((M(x+1, y)<=M(x,y)) && (M(x-1, y) <=M(x,y)))
                   Img(x,y)=1;
               end
           elseif(alpha4(x,y)==45)
               if((M(x+1, y-1)<=M(x,y))&&(M(x-1, y+1)<=M(x,y)))
                   Img(x,y)=1;
               end
           else %The angle = 0
               if((M(x, y-1)<=M(x,y))&&(M(x, y+1)<=M(x,y)))
                   Img(x,y)=1;
               end
           end
       end
   end
   Img = Img.*M;
   figure, imshow(Img), title('Non-maximal Suprression');
   
   %% Use Double Thresholding & Connectivity Analysis
 
   maxNon = max(max(Img));
   Thigh= maxNon*T;
   Tlow = Thigh*.4;
   output = zeros(size(Img));
    for y = 2:size(Img, 2)-1
       for x = 2:size(Img, 1)-1
           if(Img(x,y)>Thigh)
               output(x,y) = 1;
           elseif(Img(x,y)>Tlow)
               if(Img(x+1,y)>Thigh || Img(x+1,y+1)>Thigh || Img(x,y+1)>Thigh || Img(x-1,y)>Thigh || Img(x-1,y-1)>Thigh || Img(x,y-1)>Thigh || Img(x+1,y-1)>Thigh || Img(x-1,y+1)>Thigh)
                 output(x,y) = 1;
               end
           end
       end
    end
    figure, imshow(output, []), title('Canny'); 
   
end 