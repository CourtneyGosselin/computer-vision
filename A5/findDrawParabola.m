clear; clc; close all;
I = imread('gateway_arch.jpg');
figure, imshow(I), title('Original');
E = edge(I, 'canny',0.3,0.9);
figure, imshow(E,[]);
%% choose parabola sizes to try
C = 0.01:0.001:0.015;
c_length = numel(C);
[M,N] = size(I);
%% accumulator array H(N,M,C) initialized with zeros
H = zeros(M,N,c_length);
%% vote to fill H
[y_edge, x_edge] = find(E); % get edge points
for i = 1:length(x_edge) % for all edge points
    for c_idx=1:c_length % for all c
        for a = 1:N
            b = round(y_edge(i)-C(c_idx)*(x_edge(i)-a)^2);
            if(b < M && b >= 1) H(b,a,c_idx)=H(b,a,c_idx)+1; 
            end
        end
    end
end
%% show only third slice of H
figure, imshow(H(:,:,3),[]);
title(sprintf('Slice of H at C = %f', C(3)));

%%Find local Maxima
localMaxima = houghpeaks(H(:,:,1), 1);
%%Draw Parabolas
%y=-(x-1)^2+1
figure, imshow(I), title('test');
w = 9.5;
for x = -17:.1:17
    y = (x.^2)+localMaxima(1,1);
    rectangle('Position', [w*x+localMaxima(1,2), y, 2, 2], 'EdgeColor', 'r', 'FaceColor', 'r');
        
end
    
