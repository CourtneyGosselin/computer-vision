%% Question 2
I = imread('fig1039a.png');
%figure, imshow(I), title('Original');
BW = edge(I, 'canny', 0.5);
[centers, radii] = imfindcircles(BW, [35, 130], 'EdgeThreshold', 0.25);
figure, imshow(I), title("r1 = "+num2str(radii(1),'%12.1f')+" at ("+num2str(centers(1,2),'%12.0f')+","+num2str(centers(1,1),'%12.0f')+") r2 = "+num2str(radii(2),'%12.1f')+" at ("+num2str(centers(2,2),'%12.0f')+","+num2str(centers(2,1),'%12.0f')+")");
viscircles(centers, radii, 'EdgeColor', 'r');
